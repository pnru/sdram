#!/bin/sh

# Build EVMBUG system for ULX3S board

# synthesise design
yosys -q -p "synth_ecp5 -json sys.json" sys.v tms99000.v tms9902.v rom.v ram.v sdram.v pll.v

# place & route
# assumes 25F device
nextpnr-ecp5 --25k --package CABGA381 --json sys.json --lpf ulx3s.lpf --textcfg sys.cfg --seed 37

# pack bitstream
# idcode only needed when sending bitstream to 12F devices
ecppack  sys.cfg sys.bit --idcode 0x21111043

# send to ULX3S board (store in configuration RAM)
#ujprog sys.bit
