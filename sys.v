module sys
(
  input  wire        clk_25mhz,
  output wire [7:0]  led,
  output wire        ftdi_rxd,
  input  wire        ftdi_txd,
  
  //sdram
  output wire        sdram_clk,
  output wire        sdram_cke,
  output wire        sdram_csn,
  output wire        sdram_wen,
  output wire        sdram_rasn,
  output wire        sdram_casn,
  output wire  [1:0] sdram_ba,
  output wire [12:0] sdram_a,
  inout  wire [15:0] sdram_d,
  output wire  [1:0] sdram_dqm
);

  // reset for 255 raw clocks
  reg [7:0] ctr = 0;
  always @(posedge clk_25mhz) if (reset) ctr <= ctr+1;
  wire reset = !(&ctr);
  
  // set up clocks
  wire clk_cpu, clk_mem, clk_sdr;
  PLL pll(
    .clkin(clk_25mhz),
    .pll_25(clk_cpu),
    .pll_50(clk_mem),
    .pll125(clk_sdr),
    //.dly125(sdram_clk)
  );
  
  wire rd, wr;
  wire [15:0] ab, db_out, db_in, rom_o, ram_o, sdr_o;
  wire [3:0] bst;
  wire nmemen = bst[3];
  
  wire ram_sel = (ab[15] && ab[15:12]!=4'b1111);
  wire rom_sel = !ab[15];
  wire aca_sel = (ab[15:6]==0);
  wire sdr_sel = (ab[15:12]==4'b1111);
  
  wire cruin, cruout, cruclk;
  wire nrts, ncts;
  wire rqrd, rqwr, rqrdy, rqack;
  wire [3:0] sdr_cmd;
  
  wire int = 0, nmi = 0, hold = 0;
  wire rdy;

  // connect up circuit
  tms99000 cpu(clk_cpu, reset, ab, db_in, db_out, rd, wr, rdy, /*iaqs*/, /*as*/, int, 4'h0, nmi, cruin, cruout, cruclk, hold, bst);
  RAM      ram(clk_cpu, !ram_sel, !wr, ab[12:1], db_out, ram_o);
  ROM      rom(clk_cpu, !rom_sel,      ab[12:1], rom_o);
  tms9902  aca(clk_cpu, nrts, 1'b0 /*dsr*/, ncts, /*int*/, !aca_sel, cruout, cruin, cruclk, ftdi_rxd, ftdi_txd, ab[5:1]);
  SDRAM    sdr(clk_sdr, rqrd, rqwr, rqrdy, rqack, {9'h0,ab[15:1]}, db_out, sdr_o, sdr_cmd, sdram_ba, sdram_a, sdram_d, sdram_dqm);
  
  // sdram controller interface
  // delay clock to sdram chip by 1.5ns
  //DELAYG #(.DEL_VALUE(60)) clk_dly(clk_sdr, sdram_clk);
  assign sdram_clk  = clk_sdr;
  assign sdram_cke  = !reset;
  assign sdram_csn  = sdr_cmd[3];
  assign sdram_wen  = sdr_cmd[2];
  assign sdram_rasn = sdr_cmd[1];
  assign sdram_casn = sdr_cmd[0];
  assign rqrd  = rd & sdr_sel;
  assign rqwr  = wr & sdr_sel;
  assign rqack = !(rd|wr);

  // databus switch & misc.
  assign rdy   = (sdr_sel & !nmemen) ? rqrdy : 1;
  assign db_in = (ram_sel) ? ram_o : (rom_sel) ? rom_o : sdr_o;
  
  assign ncts  = nrts;
  assign led   = ab[8:1];

endmodule

